This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

1. npx create-next-app@latest
1. On the options select No src/ directory
1. npm run dev
1. Stop dev server
1. npm init builder.io@latest
1. npm run dev
1. Local: http://localhost:3000
1. Configure project to connect with Builder.io

## Development server
First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.tsx`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.

## Integrate pages
### Setting the model preview URL

1. Go to Models and choose the Page model.
1. Set the Preview URL to http://localhost:<your-port>, where <your-port> is the port your app is using. Be sure to include the http://.
1. Click Save.

### Creating a Builder Page

1. Go to Content.
1. Click the +New button near the top right of the screen.
1. If you have more than one model, select the one you created
1. Create a new page in Builder and name it Test Page. Notice that Builder automatically generates the path as /test-page.
1. When you are prompted to choose a starting template, choose Blank. The editor for your new page loads automatically.
1. In your new page, drag in a Text block.
1. Click Edit and add something like, "I did it!!".
1. Click the Publish button in the upper right of the browser.

### Add Builder component to your app
1. Inside of [...page], create a file called page.tsx and paste in the following:
```
export default async function Page(props: PageProps) {
  const content = await builder
    // Get the Page model from Builder with the specified options
    .get("home-page", {
      userAttributes: {
        // Use the page path specified in the URL to fetch the content
        urlPath: "/" + (props?.params?.page?.join("/") || ""),
    ........
```
* Each model page has a unique identifier and you can see it in the Models section -> your model -> advanced
* Within each model you can have pages with their associated route
