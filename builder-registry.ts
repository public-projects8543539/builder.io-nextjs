"use client";
import { Builder } from "@builder.io/react";
import Counter from "./components/Counter/Counter";
import Mapa from "./components/IframeMapa/Mapa";

Builder.registerComponent(Counter, {
  name: "Counter",
  inputs: [
    {
      name: "initialCount",
      type: "number",
    },
  ],
});

Builder.registerComponent(Mapa, {
  name: "Mapa",
});
